import React from 'react';
import './pure-min.css';
import './side-menu.css';

import Menu from './componentes/Menu';

class App extends React.Component {
  
  render(){
    return (
      <div id="layout">
        <a href="#menu" id="menuLink" className="menu-link">
            <span></span>
        </a>
        <div id="menu">
            <Menu />
        </div>
        {this.props.children}
      </div>
    );
  }
}

export default App;
