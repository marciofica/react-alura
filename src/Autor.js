import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from './componentes/InputCustomizado';
import InputSubmitCustomizado from './componentes/InputSubmitCustomizado'
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros'

class FormularioAutor extends Component {
    constructor() {
        super();
        this.state = {nome:'', email:'', senha:''};
        this.enviaForm = this.enviaForm.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    };
    
    enviaForm(evento){
        evento.preventDefault();
        $.ajax({
          url: "http://localhost:8080/api/autores",
          contentType: "application/json",
          dataType: "json",
          type: "post",
          data: JSON.stringify({nome:this.state.nome, email:this.state.email, senha:this.state.senha}),
          success: function(resposta){
            PubSub.publish('atualiza-lista-autores', resposta);
            this.setState({nome:'',email:'',senha:''});
          }.bind(this),
          error(resposta){
            if(resposta.status === 400) {
                new TratadorErros().publicaErros(resposta.responseJSON);
            }
          },
          beforeSend: function(){
            PubSub.publish("limpa-erros",{});
          }
        });
    }
    
    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.id;
        this.setState({ [name]: value });
    }

    render() {
        return(
            <div className="pure-form pure-form-aligned">
                <form className="pure-form pure-form-aligned" onSubmit={this.enviaForm} method="post">
                    <InputCustomizado id="nome" type="text" name="nome" value={this.state.nome} onChange={this.handleInputChange} label="Nome"/>
                    <InputCustomizado id="email" type="email" name="email" value={this.state.email} onChange={this.handleInputChange} label="Email"/>
                    <InputCustomizado id="senha" type="password" name="senha" value={this.state.senha} onChange={this.handleInputChange} label="Senha"/>
                    <InputSubmitCustomizado acao="Gravar" />
                </form>
            </div>
        );
    }
}

class TabelaAutores extends Component {
    render() {
        return(
            <div>            
                <table className="pure-table pure-table-striped">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>email</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.lista.map(function(autor){
                        return (
                            <tr key={autor.id}>
                            <td>{autor.nome}</td>
                            <td>{autor.email}</td>
                            </tr>
                        );
                        })
                    }
                    </tbody>
                </table> 
            </div> 
        );
    }
}

export default class AutorBox extends Component {
    constructor() {
        super();
        this.state = {lista : []};
    }
    componentDidMount(){
        $.ajax({
            url:"http://localhost:8080/api/autores",
            dataType: 'json',
            success:function(resposta){
                this.setState({lista:resposta});
                }.bind(this)
        });

        PubSub.subscribe('atualiza-lista-autores', function(topico,novaLista){
            this.setState({lista:novaLista});
        }.bind(this));
    }

    render() {
        return(
            <div id="main">
                <div className="header">
                    <h1>/Autores</h1>
                </div>
                <div className="content" id="content">                    
                    <FormularioAutor />
                    <TabelaAutores lista={this.state.lista} />
                </div>
            </div>           
        );
    }
}