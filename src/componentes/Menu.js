import React, {Component} from 'react';
import $ from 'jquery';
import PubSub from 'pubsub-js'
import { Link } from "react-router-dom";

export default class Menu extends Component {
    constructor(){
        super();
        this.state = {qtdAutores: 0, qtdLivros: 0};
    }

    componentDidMount() {
        $.ajax({
            url:"http://localhost:8080/api/autores",
            dataType: 'json',
            success:function(resposta){
                this.setState({qtdAutores:resposta.length});
            }.bind(this)
        });

        $.ajax({
            url:"http://localhost:8080/api/livros",
            dataType: 'json',
            success:function(resposta){
                this.setState({qtdLivros:resposta.length});
            }.bind(this)
        });

        PubSub.subscribe('atualiza-lista-autores', function(topico,novaLista){
            this.setState({qtdAutores:novaLista.length});
        }.bind(this));

        PubSub.subscribe('atualiza-lista-livros', function(topico,novaLista){
            this.setState({qtdLivros:novaLista.length});
        }.bind(this));
    }

    render(){
        return(
            <div className="pure-menu">
                <a className="pure-menu-heading" href="/company">Company</a>
                <ul className="pure-menu-list">
                    <li className="pure-menu-item"><Link to="/" className="pure-menu-link">Home</Link></li>
                    <li className="pure-menu-item"><Link to="/autores" className="pure-menu-link">Autor ({this.state.qtdAutores})</Link></li>
                    <li className="pure-menu-item"><Link to="/livros" className="pure-menu-link">Livro ({this.state.qtdLivros})</Link></li>
                </ul>
            </div>
        );
    }
}